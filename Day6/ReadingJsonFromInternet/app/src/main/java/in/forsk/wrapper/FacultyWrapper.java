package in.forsk.wrapper;

import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by Saurabh on 3/28/2016.
 */
//In general, a wrapper class is any class which "wraps" or "encapsulates" the functionality of another class
//or component. These are useful by providing a level of abstraction from the
//implementation of the underlying class or component
//http://www.javawithus.com/tutorial/wrapper-classes
public class FacultyWrapper {

    private String first_name = "";
    private String last_name = "";
    private String photo = "";
    private String department = "";
    private String reserch_area = "";

    ArrayList<String> interest_areas = new ArrayList<String>();
    ArrayList<String> contact_details = new ArrayList<String>();


    public FacultyWrapper() {
        // TODO Auto-generated constructor stub
    }

    public FacultyWrapper(JSONObject jObj) {
        try {
            //Paste your code here for constructor.

            System.out.println(jObj.toString());

            first_name = jObj.getString("first_name");
            last_name = jObj.getString("last_name");
            photo = jObj.getString("photo");
            department = jObj.getString("department");
            reserch_area = jObj.getString("reserch_area");


            JSONObject interest_areas = jObj.getJSONObject("interest_areas");
            JSONObject contact_details = jObj.getJSONObject("contact_details");

            this.interest_areas.add(interest_areas.getString("subject_1"));
            this.interest_areas.add(interest_areas.getString("subject_2"));
            this.interest_areas.add(interest_areas.getString("subject_3"));

            this.contact_details.add(contact_details.getString("phone"));
            this.contact_details.add(contact_details.getString("email"));

        } catch (Exception e) {
            // TODO: handle exception
        }
    }


    public String getFirst_name() {
        return first_name;
    }

    public String getLast_name() {
        return last_name;
    }

    public String getPhoto() {
        return photo;
    }

    public String getDepartment() {
        return department;
    }

    public String getReserch_area() {
        return reserch_area;
    }

    public ArrayList<String> getInterest_areas() {
        return interest_areas;
    }

    public ArrayList<String> getContact_details() {
        return contact_details;
    }

    public void setFirst_name(String first_name) {
        this.first_name = first_name;
    }

    public void setLast_name(String last_name) {
        this.last_name = last_name;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public void setDepartment(String department) {
        this.department = department;
    }

    public void setReserch_area(String reserch_area) {
        this.reserch_area = reserch_area;
    }

    public void setInterest_areas(ArrayList<String> interest_areas) {
        this.interest_areas = interest_areas;
    }

    public void setContact_details(ArrayList<String> contact_details) {
        this.contact_details = contact_details;
    }
}
